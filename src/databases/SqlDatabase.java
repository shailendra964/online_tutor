package databases;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlDatabase {
	
	/**
	 * @param url
	 * @param user
	 * @param password
	 * @param con
	 */
	private String url;
	private String user;
	private String password;
	private Connection con;	
	
	public SqlDatabase(String url, String user, String password) {
	
		this.url = url;
		this.user = user;
		this.password = password;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			try {
				con= DriverManager.getConnection(this.url, this.user, this.password);
			} catch (SQLException e) {
			
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	public Connection getConnection()
	{
		return this.con;
	}
	
	public void closeConnection()
	{
		try {
			this.con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
