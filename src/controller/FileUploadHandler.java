package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.util.Date;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet("/FileUploadHandler")
public class FileUploadHandler extends HttpServlet {
        /**
	 * 
	 */

		private final String UPLOAD_DIRECTORY = "D:/tutor";
      
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
          Date d = new Date();
          
            //process only if its multipart content
            if(ServletFileUpload.isMultipartContent(request)){
                try {
                    List<FileItem> multiparts = new ServletFileUpload(
                                             new DiskFileItemFactory()).parseRequest(request);
                  
                    for(FileItem item : multiparts){
                        if(!item.isFormField()){
                            String name = new File(item.getName()).getName();
                            //String fileName = name + d;
                       // System.out.println(d);
                            item.write( new File(UPLOAD_DIRECTORY + File.separator + name));
                            System.out.println("Copied");
                        }
                    }
               
                   //File uploaded successfully
                   request.setAttribute("message", "File Uploaded Successfully");
                } catch (Exception ex) {
                   request.setAttribute("message", "File Upload Failed due to " + ex);
                }          
             
            }else{
                request.setAttribute("message",
                                     "Sorry this Servlet only handles file upload request");
                System.out.println("Not Copied");
            }
        
            request.getRequestDispatcher("tutor/File_Upload.jsp").forward(request, response);
         
        }
      
    }
