package listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import databases.SqlDatabase;
/**
 * Application Lifecycle Listener implementation class JdbcConfig
 *
 */
@WebListener
public class JdbcConfig implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public JdbcConfig() {
     
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
     
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
       
    	ServletContext sc =  sce.getServletContext();
    	String user= sc.getInitParameter("username");
    	String password= sc.getInitParameter("password");
    	String url= sc.getInitParameter("url");
    	
    	SqlDatabase sd = new SqlDatabase(url, user, password);
    	sc.setAttribute("SqlDb", sd);
    }
	
}
