package filters;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import databases.SqlDatabase;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import com.mysql.jdbc.PreparedStatement;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/LoginFilter")
public class LoginFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		String query = "select * from adminlogin where name=? and password =?";
		ServletContext sc = request.getServletContext();
		SqlDatabase db = (SqlDatabase)sc.getAttribute("SqlDb");
		Connection con = db.getConnection();
		
		try {
		PreparedStatement ps=(PreparedStatement)con.prepareStatement(query);
		ps.setString(1, "admin");
		ps.setString(2, "admin");
		ResultSet rs = ps.executeQuery();
			
		if(rs.next())
		{
			System.out.println(" rs"+rs.getString(2));
		}
		
		
		
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	
	}

}
