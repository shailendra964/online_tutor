package filters;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/LoginFilter")
public class ValidationFilter implements Filter {

    /**
     * Default constructor. 
     */
    public ValidationFilter() {
       
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		
		String nameError="";
		String passwordError="";
		
		boolean flag = true;
		
		if(name.equals(""))
		{
			nameError = "Name field should not be empty.";
			flag=false;
		}
		else
		{
			nameError = name;
		}
		
		if(password.equals("") || password == null)
		{
			passwordError = "Password field should not be empty.";
			flag=false;
		}
		
		if(flag==true)
		{
			chain.doFilter(request, response);
		}
		else {
			out.println("<html>");
			out.println("<body>");
			out.println("<h2> Login form </h2>");
			out.println("<form method='POST' action = './LoginFilter'> ");
			out.println("<input type='text' name='name' value='"+nameError+"'>");
			out.println("<input type='text' name='password' value='"+passwordError+"'>");
			out.println("<input type='submit' value='submit'>");
			out.println("</form>");
			out.println("</body>");
			out.println("<html>");
			
			
		}
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	
	}

}
