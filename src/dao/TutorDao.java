/**
 * 
 */
package dao;

import java.util.List;

/**
 * @author shailendrasi
 *
 */
public interface TutorDao {

	public int save(Object o) throws Exception;
	public int update(Object o) throws Exception;
	public int delete(Object o) throws Exception;
	public List<Object> findByName(Object o) throws Exception;
	public List<Object> findAll(Object o) throws Exception;
	
}
